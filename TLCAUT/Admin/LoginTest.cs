﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.Admin
{
    [TestFixture, Order(1)]
    class LoginTest
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
           driver = new ChromeDriver();
           driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
           driver.Manage().Window.Maximize();
        }

        [Test, Order(1)]
        public void TestA()
        {
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();

            Assert.AreEqual("TLC BMS Dashboard", driver.Title);


        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}