﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.Material
{
    [TestFixture, Order(1)]
    class MaterialCreate
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Materials" menu item and click on it
            IWebElement MaterialList = driver.FindElement(By.LinkText("Materials"));
            MaterialList.Click();

            // Begin adding a Material by clicking on the "Add New Material" link
            IWebElement MaterialAdd = driver.FindElement(By.LinkText("Add New Material"));
            MaterialAdd.Click();

            // Add a Material Name
            IWebElement MaterialAddName = driver.FindElement(By.Id("Input_Name"));
            MaterialAddName.Clear();
            MaterialAddName.SendKeys("Automated Test Material");

            // Add a Material description
            IWebElement MaterialDescription = driver.FindElement(By.Id("Input_Description"));
            MaterialDescription.Clear();
            MaterialDescription.SendKeys("A material generated through automated testing.");

            // Set this Product to be a TLC Product
            IWebElement MaterialCategory = driver.FindElement(By.Id("Input_Type"));
            SelectElement selectElement = new SelectElement(MaterialCategory);
            selectElement.SelectByText("Packaging");

            // Submit the DC info and create it
            IWebElement submit = driver.FindElement(By.ClassName("btn-primary"));
            submit.Click();

            IWebElement MaterialTitle = driver.FindElement(By.TagName("h2"));

            String MaterialText = MaterialTitle.Text;

            Assert.AreEqual("Automated Test Material", MaterialText);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
