﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.Material
{
    [TestFixture, Order(2)]
    class MaterialEdit
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Distribution Centers" menu item and click on it
            IWebElement MaterialList = driver.FindElement(By.LinkText("Materials"));
            MaterialList.Click();

            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("AutomatedTestMaterialdetails")));

            IWebElement MaterialEdit = driver.FindElement(By.Id("AutomatedTestMaterialdetails"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            MaterialEdit.Click();

            var wait2 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element2 = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("bQuickEdit")));

            IWebElement MaterialEdit2 = driver.FindElement(By.Id("bQuickEdit"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            MaterialEdit2.Click();

            // Edit the Material Name
            IWebElement MaterialAddName = driver.FindElement(By.Id("Material_Name"));
            MaterialAddName.Clear();
            MaterialAddName.SendKeys("Automated Test Material 2");

            // Edit the Material Description
            IWebElement MaterialDescription = driver.FindElement(By.Id("Material_Description"));
            MaterialDescription.Clear();
            MaterialDescription.SendKeys("An updated description for a material generated through automated testing.");

            // Submit the Material info and finish editing
            IWebElement submit = driver.FindElement(By.ClassName("btn-primary"));
            submit.Click();

            IWebElement MaterialTitle = driver.FindElement(By.TagName("h2"));

            String MaterialText = MaterialTitle.Text;

            Assert.AreEqual("Automated Test Material 2", MaterialText);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
