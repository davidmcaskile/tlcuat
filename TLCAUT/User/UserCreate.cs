﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.User
{
    [TestFixture, Order(1)]
    class UserCreate
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            // Find the "Administration" menu item and click on it
            IWebElement User = driver.FindElement(By.LinkText("Administration"));
            User.Click();

            // Find the "Users" menu item and click on it
            IWebElement UserList = driver.FindElement(By.LinkText("Users"));
            UserList.Click();

            // Begin adding a User by clicking on the "Add a New User" link
            IWebElement UserAdd = driver.FindElement(By.LinkText("Add a New User"));
            UserAdd.Click();

            // Add a User First Name
            IWebElement UserAddFirstName = driver.FindElement(By.Id("Input_FirstName"));
            UserAddFirstName.Clear();
            UserAddFirstName.SendKeys("Jean");

            // Add a User last name
            IWebElement UserAddLastName = driver.FindElement(By.Id("Input_LastName"));
            UserAddLastName.Clear();
            UserAddLastName.SendKeys("Grey");

            // Add an email address
            IWebElement UserAddEmail = driver.FindElement(By.Id("Input_Email"));
            UserAddEmail.Clear();
            UserAddEmail.SendKeys("jg@tlcpetfood.com");

            // Add the password
            IWebElement UserAddPass = driver.FindElement(By.Id("Input_NewPassword"));
            UserAddPass.Clear();
            UserAddPass.SendKeys("Tlc12345!");

            // Confirm the password
            IWebElement UserConfirmPass = driver.FindElement(By.Id("Input_ConfirmPassword"));
            UserConfirmPass.Clear();
            UserConfirmPass.SendKeys("Tlc12345!");

            // Add a Role
            IWebElement UserRole = driver.FindElement(By.ClassName("addRole"));
            UserRole.Click();

            // Submit the User info and create it
            IWebElement submit = driver.FindElement(By.ClassName("btn-primary"));
            submit.Click();

            //IWebElement DCTitle = driver.FindElement(By.TagName("h2"));

           // String DCText = DCTitle.Text;

           // Assert.AreEqual("Automated Test DC", DCText);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Close();
        }

    }
}
