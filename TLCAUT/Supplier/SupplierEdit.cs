﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.Supplier
{
    [TestFixture, Order(2)]
    class SupplierEdit
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test, Order(3)]
        public void TestC()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Distribution Centers" menu item and click on it
            IWebElement DCList = driver.FindElement(By.LinkText("Suppliers"));
            DCList.Click();

            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("AutomatedTestSupplierdetails")));

            IWebElement SupplierEdit = driver.FindElement(By.Id("AutomatedTestSupplierdetails"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            SupplierEdit.Click();

            var wait2 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element2 = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("bQuickEdit")));

            IWebElement SupplierEdit2 = driver.FindElement(By.XPath("/html/body/div[2]/main/div/div[4]/div/div[1]/div[1]/div/div/div[1]/div/div/a/span"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SupplierEdit2.Click();
            SupplierEdit2.Click();

            // Edit the name of the Supplier
            IWebElement SupplierName = driver.FindElement(By.Id("Supplier_Name"));
            SupplierName.Clear();
            SupplierName.SendKeys("Automated Test Supplier 2");

            // Submit the Supplier info and create it
            IWebElement submit = driver.FindElement(By.ClassName("btn-primary"));
            submit.Click();

            var wait3 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element3 = wait3.Until(ExpectedConditions.ElementIsVisible(By.TagName("h2")));

            IWebElement Title = driver.FindElement(By.TagName("h2"));

            String SupplierText = Title.Text;

            // Check that the test has been redirected to the Details page after creation and is showing the right Supplier Name
            Assert.AreEqual("Automated Test Supplier 2", SupplierText);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
