﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.Supplier
{
    [TestFixture, Order(1)]
    class SupplierCreate
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test, Order(2)]
        public void TestB()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Suppliers" menu item and click on it
            IWebElement SupplierList = driver.FindElement(By.LinkText("Suppliers"));
            SupplierList.Click();

            // Begin adding a Supplier by clicking on the "Add New Supplier" link
            IWebElement SupplierAdd = driver.FindElement(By.LinkText("Add New Supplier"));
            SupplierAdd.Click();

            // Add a name to the Supplier
            IWebElement SupplierName = driver.FindElement(By.Id("Supplier_Name"));
            SupplierName.Clear();
            SupplierName.SendKeys("Automated Test Supplier");

            // Add a description to the Supplier
            IWebElement SupplierDescription = driver.FindElement(By.Id("Supplier_Description"));
            SupplierDescription.Clear();
            SupplierDescription.SendKeys("This is a test supplier created throguh automated testing.");

            // Add an address name for the Supplier
            IWebElement SupplierAddressName = driver.FindElement(By.Id("SupplierAddress_Name"));
            SupplierAddressName.Clear();
            SupplierAddressName.SendKeys("Head Office");

            // Add Street 1 for the Supplier Address
            IWebElement SupplierStreet = driver.FindElement(By.Id("Street1"));
            SupplierStreet.Clear();
            SupplierStreet.SendKeys("806 Gordon St.");

            // Add City for the Supplier Address
            IWebElement SupplierCity = driver.FindElement(By.Id("City"));
            SupplierCity.Clear();
            SupplierCity.SendKeys("Guelph");

            // Select the Province for the Address
            IWebElement SupplierProvince = driver.FindElement(By.Id("ProvinceID"));
            SelectElement selectElement = new SelectElement(SupplierProvince);
            selectElement.SelectByText("Ontario");

            // Add payment terms for the supplier
            IWebElement SupplierPostal = driver.FindElement(By.Id("PostalCode"));
            SupplierPostal.Clear();
            SupplierPostal.SendKeys("N1G1Y7");

            // Submit the DC info and create it
            IWebElement submit = driver.FindElement(By.ClassName("btn-primary"));
            submit.Click();

            var wait2 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element2 = wait2.Until(ExpectedConditions.ElementIsVisible(By.TagName("h2")));

            IWebElement Title = driver.FindElement(By.TagName("h2"));

            String SupplierText = Title.Text;

            // Check that the test has been redirected to the Details page after creation and is showing the right Supplier Name
            Assert.AreEqual("Automated Test Supplier", SupplierText);

        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }
    }
}
