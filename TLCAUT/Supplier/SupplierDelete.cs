﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.Supplier
{
    [TestFixture, Order(4)]
    class SupplierDelete
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test, Order(4)]
        public void TestD()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Suppliers" menu item and click on it
            IWebElement SupplierList = driver.FindElement(By.LinkText("Suppliers"));
            SupplierList.Click();

            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("AutomatedTestSupplier2delete")));

            // Click on the Delete icon for the supplier
            IWebElement SupplierDelete = driver.FindElement(By.Id("AutomatedTestSupplier2delete"));
            SupplierDelete.Click();
            SupplierDelete.Click();

            var element2 = wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("btn-danger")));

            // Confirm you want to delete the Supplier
            IWebElement submit = driver.FindElement(By.ClassName("btn-danger"));
            submit.Click();
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
