﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.Extensions;
using System.Threading;

namespace TLCAUT.Orders
{
    [TestFixture, Order(2)]
    class OrderCreateRetail
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "https://tlcbms.test.sandboxsoftware.ca";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            
                // Go to customer profile
                driver.Url = "https://tlcbms.test.sandboxsoftware.ca/CRM/Customers/Details?id=95#divDetails";

                Thread.Sleep(2000);

                // Go to the order tab
                IWebElement OrderTab = driver.FindElement(By.LinkText("Orders"));
                OrderTab.Click();

                Thread.Sleep(2000);

                var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
                var element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("btn-add")));

                Thread.Sleep(2000);

                // start the order
                IWebElement OrderAdd = driver.FindElement(By.Id("btn-add"));
                OrderAdd.Click();

                var wait2 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
                var element2 = wait2.Until(ExpectedConditions.ElementIsVisible(By.Id("toOrderShipping")));

                Thread.Sleep(2000);

                // Select the source for the order
                IWebElement OrderSource = driver.FindElement(By.Id("source"));
                SelectElement selectElement2 = new SelectElement(OrderSource);
                selectElement2.SelectByText("Email");

                Thread.Sleep(2000);

                // Select the first product
                IWebElement OrderProduct1 = driver.FindElement(By.Name("LineItems[0].ProductID"));
                SelectElement selectElement = new SelectElement(OrderProduct1);
                selectElement.SelectByText("[ID:5] 35 LB Whole Life Dog Food Case (Food Product)");

                Thread.Sleep(2000);

                // Enter the quantity for the first product
                IWebElement OrderProduct1Quantity = driver.FindElement(By.Name("LineItems[0].Quantity"));
                OrderProduct1Quantity.Clear();
                OrderProduct1Quantity.SendKeys("10");

                Thread.Sleep(2000);

                // Add another line item
                IWebElement OrderAddLine = driver.FindElement(By.ClassName("add"));
                OrderAddLine.Click();

                Thread.Sleep(2000);

                // Select the second product
                IWebElement OrderProduct2 = driver.FindElement(By.Name("LineItems[0].ProductID"));
                SelectElement selectElement3 = new SelectElement(OrderProduct2);
                selectElement3.SelectByText("[ID:10] 35 LB Whole Life Puppy Food Case (Food Product)");

                Thread.Sleep(2000);

                // Enter the quantity for the second product
                IWebElement OrderProduct2Quantity = driver.FindElement(By.Name("LineItems[0].Quantity"));
                OrderProduct2Quantity.Clear();
                OrderProduct2Quantity.SendKeys("10");

                Thread.Sleep(2000);

                // Submit the Product info and create it
                IWebElement Next1 = driver.FindElement(By.Id("toOrderShipping"));
                Next1.Click();

                Thread.Sleep(2000);

                var wait3 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
                var element3 = wait3.Until(ExpectedConditions.ElementIsVisible(By.Id("distribution-center")));

                Thread.Sleep(2000);

                // Select the DC
                IWebElement OrderDC = driver.FindElement(By.Id("distribution-center"));
                SelectElement selectElement4 = new SelectElement(OrderDC);
                selectElement4.SelectByText("New Hamburg");

                Thread.Sleep(2000);

                // Select the SP
                IWebElement OrderSP = driver.FindElement(By.Id("courier"));
                SelectElement selectElement5 = new SelectElement(OrderSP);
                selectElement5.SelectByText("Canada Post");

                Thread.Sleep(2000);

                // Submit the Product info and create it
                IWebElement Next2 = driver.FindElement(By.Id("toOrderBilling"));
                Next2.Click();

                Thread.Sleep(4000);

                // Select the Payment Method
                IWebElement OrderPM = driver.FindElement(By.Id("Payments_0__CombinedCustomerPaymentMethodID"));
                SelectElement selectElement6 = new SelectElement(OrderPM);
                selectElement6.SelectByText("Cash");

                Thread.Sleep(2000);

                // Submit the Product info and create it
                IWebElement Next3 = driver.FindElement(By.Id("toOrderPayment"));
                Next3.Click();

                Thread.Sleep(2000);

                // Submit the Product info and create it
                IWebElement Next4 = driver.FindElement(By.Id("completeOrder"));
                Next4.Click();

            
            //IWebElement ProductTitle = driver.FindElement(By.TagName("h2"));

            //String ProductText = ProductTitle.Text;

            //Assert.AreEqual("Automated Test Product", ProductText);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
