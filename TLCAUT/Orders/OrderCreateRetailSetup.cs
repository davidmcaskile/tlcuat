﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.Extensions;
using System.Threading;

namespace TLCAUT.Orders
{
    [TestFixture, Order(1)]
    class OrderCreateRetailSetup
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "https://tlcbms.test.sandboxsoftware.ca";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            
            // Go to customer profile
            driver.Url = "https://tlcbms.test.sandboxsoftware.ca/CRM/Customers/Details?id=133";

            Thread.Sleep(2000);

            // Go to the Addresses tab
            IWebElement AddressTab = driver.FindElement(By.LinkText("Addresses"));
            AddressTab.Click();

            Thread.Sleep(2000);

            //var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            //var element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@href='https://tlcbms.test.sandboxsoftware.ca/CRM/Customers/Addresses/Edit?id=121']")));

            Thread.Sleep(2000);

            // Edit an address
            IWebElement AddressEdit = driver.FindElement(By.XPath("/html/body/div[2]/main/div[1]/div[4]/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/a[1]"));
            AddressEdit.Click();

            var wait2 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            wait2.Until(ExpectedConditions.ElementIsVisible(By.Id("FirstName")));

            // Set the address first name
            IWebElement FirstName = driver.FindElement(By.Id("FirstName"));
            FirstName.Clear();
            FirstName.SendKeys("Selenium");

            // Set the address last name
            IWebElement LastName = driver.FindElement(By.Id("LastName"));
            LastName.Clear();
            LastName.SendKeys("Address");

            // Set the address phone #
            IWebElement Phone = driver.FindElement(By.Id("Phone"));
            Phone.Clear();
            Phone.SendKeys("1234567890");

            // Set Street 1
            IWebElement Street1 = driver.FindElement(By.Id("Street1"));
            Street1.Clear();
            Street1.SendKeys("500 Fairway Rd. S");

            

            // Set the city
            IWebElement City = driver.FindElement(By.Id("City"));
            City.Clear();
            City.SendKeys("Kitchener");

            // Select the province
            IWebElement Province = driver.FindElement(By.Id("ProvinceID"));
            SelectElement selectElement = new SelectElement(Province);
            selectElement.SelectByText("Ontario");

            // Set the postal
            IWebElement PostalCode = driver.FindElement(By.Id("PostalCode"));
            PostalCode.Clear();
            PostalCode.SendKeys("N2C1X3");

            Thread.Sleep(2000);

          
            Thread.Sleep(2000);

          

            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("dx-overlay-content dx-toast-success dx-toast-content dx-resizable")));

            // Enter the quantity for the first product
            IWebElement Save = driver.FindElement(By.XPath("/html/body/div[2]/div/main/form/div[14]/button"));
            Save.Click();

            var wait4 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            wait4.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("dx-overlay-content dx-toast-success dx-toast-content dx-resizable")));

            Thread.Sleep(2000);

            Save.Click();

            Thread.Sleep(2000);

            var wait3 = new WebDriverWait(driver, new TimeSpan(0, 5, 0));
            wait3.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader")));

            // Go to the Addresses tab
            IWebElement BillingTab = driver.FindElement(By.LinkText("Billing"));
            BillingTab.Click();

            var wait6 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            wait6.Until(ExpectedConditions.ElementExists(By.XPath("/html/body/div[2]/main/div[1]/div[4]/div/div[1]/div[6]/div/div/div/a[1]")));

            IWebElement AddBilling = driver.FindElement(By.XPath("/html/body/div[2]/main/div[1]/div[4]/div/div[1]/div[6]/div/div/div/a[1]"));
            AddBilling.Click();

            var wait5 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            wait5.Until(ExpectedConditions.ElementExists(By.Id("PaymentMethodID")));

            // Select the payment method type
            IWebElement Payment = driver.FindElement(By.Id("PaymentMethodID"));
            SelectElement selectElement2 = new SelectElement(Payment);
            selectElement2.SelectByText("Credit Card");

            // Set the card name
            IWebElement CardName = driver.FindElement(By.Id("customer-payment-method-name"));
            CardName.Clear();
            CardName.SendKeys("Test Card");

            driver.SwitchTo().ParentFrame();
            driver.SwitchTo().Frame(driver.FindElement(By.XPath("/html/body/div[2]/main/div[1]/div[4]/div/div[1]/div[6]/div/div/form/div/div[2]/div[3]/div/div/div[1]/div/iframe")));

            IWebElement CardNumber = driver.FindElement(By.Name("cardnumber"));
            CardNumber.Clear();
            CardNumber.SendKeys("4000001240000000");

            IWebElement CardExp = driver.FindElement(By.Name("exp-date"));
            CardExp.Clear();
            CardExp.SendKeys("1225");

            IWebElement CardCVC = driver.FindElement(By.Name("cvc"));
            CardCVC.Clear();
            CardCVC.SendKeys("123N1G1Y7");


            Thread.Sleep(2000);

            driver.SwitchTo().ParentFrame();


            Thread.Sleep(2000);

            IWebElement IsDefault = driver.FindElement(By.Id("payment-default"));
            IsDefault.Click();

            Thread.Sleep(2000);

            IWebElement CardSave = driver.FindElement(By.Id("save-customer-payment-method"));
            CardSave.Click();

            var wait9 = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            wait9.Until(ExpectedConditions.ElementExists(By.XPath("/html/body/div[2]/main/div[1]/div[4]/div/div[1]/div[6]/div/div/h3")));

            Assert.AreEqual(true, driver.PageSource.Contains("Test Card"));
            
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
