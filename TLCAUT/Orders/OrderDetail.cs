﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.Orders
{
    [TestFixture, Order(4)]
    class OrderDetail
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Products" menu item and click on it
            IWebElement ProductList = driver.FindElement(By.LinkText("Products"));
            ProductList.Click();

            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("AutomatedTestProduct2details")));

            IWebElement ProductDetails = driver.FindElement(By.Id("AutomatedTestProduct2details"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            ProductDetails.Click();

            IWebElement ProductTitle = driver.FindElement(By.TagName("h2"));

            String ProductText = ProductTitle.Text;

            Assert.AreEqual("Automated Test Product 2", ProductText);

        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}

