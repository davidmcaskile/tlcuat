﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.Product
{
    [TestFixture, Order(1)]
    class ProductCreate
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Products" menu item and click on it
            IWebElement ProductList = driver.FindElement(By.LinkText("Products"));
            ProductList.Click();

            // Begin adding a Product by clicking on the "Add New Distribution Center" link
            IWebElement ProductAdd = driver.FindElement(By.LinkText("Add New Product"));
            ProductAdd.Click();

            // Add a Product Name
            IWebElement ProductAddName = driver.FindElement(By.Id("Input_Name"));
            ProductAddName.Clear();
            ProductAddName.SendKeys("Automated Test Product");
            
            // Set the Product Default UoM
            IWebElement ProductType = driver.FindElement(By.Id("Input_Type"));
            SelectElement selectElement = new SelectElement(ProductType);
            selectElement.SelectByText("Food Product");

            
            // Submit the Product info and create it
            IWebElement submit = driver.FindElement(By.ClassName("btn-primary"));
            submit.Click();

            IWebElement ProductTitle = driver.FindElement(By.TagName("h2"));

            String ProductText = ProductTitle.Text;

            Assert.AreEqual("Automated Test Product", ProductText);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
