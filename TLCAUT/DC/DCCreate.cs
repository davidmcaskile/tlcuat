﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.DC
{
    [TestFixture, Order(1)]
    class DCCreate
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.dev.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Distribution Centers" menu item and click on it
            IWebElement DCList = driver.FindElement(By.LinkText("Distribution Centers"));
            DCList.Click();

            // Begin adding a DC by clicking on the "Add New Distribution Center" link
            IWebElement DCAdd = driver.FindElement(By.LinkText("Add New Distribution Center"));
            DCAdd.Click();

            // Add a DC Name
            IWebElement DCAddName = driver.FindElement(By.Id("DistributionCenter_Name"));
            DCAddName.Clear();
            DCAddName.SendKeys("Automated Test DC");

            // Add Street 1 info
            IWebElement DCAddStreetName1 = driver.FindElement(By.Id("DistributionCenter_Street1"));
            DCAddStreetName1.Clear();
            DCAddStreetName1.SendKeys("102 Bedford Hwy");

            // Add DC city
            IWebElement DCAddCity = driver.FindElement(By.Id("DistributionCenter_City"));
            DCAddCity.Clear();
            DCAddCity.SendKeys("Halifax");

            // Select the province
            IWebElement DCAddProvince = driver.FindElement(By.Id("DistributionCenter_ProvinceID"));
            SelectElement selectElement = new SelectElement(DCAddProvince);
            selectElement.SelectByText("Nova Scotia");

            // Add the postal code
            IWebElement DCAddPC = driver.FindElement(By.Id("DistributionCenter_PostalCode"));
            DCAddPC.Clear();
            DCAddPC.SendKeys("B3M 3N3");

            // Select the DC type
            IWebElement DCAddType = driver.FindElement(By.Id("DistributionCenter_DistributionCenterForEnumID"));
            SelectElement selectElementType = new SelectElement(DCAddType);
            selectElementType.SelectByText("TLC Pet Food Canada");

            // Submit the DC info and create it
            IWebElement submit = driver.FindElement(By.ClassName("btn-primary"));
            submit.Click();

            IWebElement DCTitle = driver.FindElement(By.TagName("h2"));

            String DCText = DCTitle.Text;

            Assert.AreEqual("Automated Test DC", DCText);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
