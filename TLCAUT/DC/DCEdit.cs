﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.DC
{
    [TestFixture, Order(2)]
    class DCEdit
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Distribution Centers" menu item and click on it
            IWebElement DCList = driver.FindElement(By.LinkText("Distribution Centers"));
            DCList.Click();

            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("AutomatedTestDCedit")));

            IWebElement DCEdit = driver.FindElement(By.Id("AutomatedTestDCedit"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            DCEdit.Click();

            // Edit the DC Name
            IWebElement DCEditName = driver.FindElement(By.Id("DistributionCenter_Name"));
            DCEditName.Clear();
            DCEditName.SendKeys("Automated Test DC 2");

            // Reset the country because Jeff's code is garbage
            IWebElement DCCountry = driver.FindElement(By.XPath("//input[@value='1']"));
            DCCountry.Click();

            // Edit Street 1 info
            IWebElement DCEditStreetName1 = driver.FindElement(By.Id("DistributionCenter_Street1"));
            DCEditStreetName1.Clear();
            DCEditStreetName1.SendKeys("2973 Eglinton Rd.");

            // Add DC city
            IWebElement DCEditCity = driver.FindElement(By.Id("DistributionCenter_City"));
            DCEditCity.Clear();
            DCEditCity.SendKeys("Mississauga");

            // Select the province
            IWebElement DCEditProvince = driver.FindElement(By.Id("DistributionCenter_ProvinceID"));
            SelectElement selectElement = new SelectElement(DCEditProvince);
            selectElement.SelectByText("Ontario");

            // Add the postal code
            IWebElement DCEditPC = driver.FindElement(By.Id("DistributionCenter_PostalCode"));
            DCEditPC.Clear();
            DCEditPC.SendKeys("L5L 2Y2");

            // Submit the DC info and finish editing
            IWebElement submit = driver.FindElement(By.ClassName("btn-primary"));
            submit.Click();

            IWebElement DCTitle = driver.FindElement(By.TagName("h2"));

            String DCText = DCTitle.Text;

            Assert.AreEqual("Automated Test DC 2", DCText);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
