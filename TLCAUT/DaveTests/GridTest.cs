﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TLCAUT.DaveTests
{
    class GridTest
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            // Find the search element
            IWebElement SearchToggle = driver.FindElement(By.Id("sCustomerAndOrderSearch"));
            SearchToggle.Click();

            // Enter some info into the search field
            IWebElement SearchField = driver.FindElement(By.ClassName("SearchToggle"));
            SearchField.SendKeys("Brown");

            // Click on the result
            IWebElement SearchResult = driver.FindElement(By.ClassName("dx-clearfix"));
            SearchResult.Click();
            




        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
