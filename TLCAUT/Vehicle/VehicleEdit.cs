﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.Vehicle
{
    [TestFixture, Order(2)]
    class VehicleEdit
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Vehicles" menu item and click on it
            IWebElement VehicleList = driver.FindElement(By.LinkText("Vehicles"));
            VehicleList.Click();

            // wait for the link to edit the vehicle to appear on the page
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("AutomatedTestVehicleedit")));

            // Click on the link to edit the vehicle
            IWebElement VehicleEdit = driver.FindElement(By.Id("AutomatedTestVehicleedit"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            VehicleEdit.Click();

            // Edit the DC Name
            IWebElement VehicleEditName = driver.FindElement(By.Id("Input_Name"));
            VehicleEditName.Clear();
            VehicleEditName.SendKeys("Automated Test Vehicle 2");

            // Edit the mileage
            IWebElement VehicleMiles = driver.FindElement(By.Id("Input_Odometer"));
            VehicleMiles.Clear();
            VehicleMiles.SendKeys("58500");

            // Associate the vehicle with NH DC
            IWebElement VehicleDC = driver.FindElement(By.Id("Input_DistributionCenterID"));
            SelectElement selectElement = new SelectElement(VehicleDC);
            selectElement.SelectByText("Buffalo Office");

            // Submit the DC info and finish editing
            IWebElement submit = driver.FindElement(By.ClassName("btn-primary"));
            submit.Click();

            IWebElement VehicleTitle = driver.FindElement(By.TagName("h2"));

            String VehicleText = VehicleTitle.Text;

            Assert.AreEqual("Automated Test Vehicle 2", VehicleText);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
