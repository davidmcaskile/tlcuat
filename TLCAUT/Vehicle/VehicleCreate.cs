﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLCAUT.Vehicle
{
    [TestFixture, Order(1)]
    class VehicleCreate
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Create webdriver
            driver = new ChromeDriver();
            driver.Manage().Window.Position = new System.Drawing.Point(4000, 1);
            driver.Manage().Window.Maximize();

            // Go to test site
            driver.Url = "http://tlcbms.test.sandboxsoftware.ca/";

            // Enter the login email
            IWebElement email = driver.FindElement(By.Id("Input_Email"));
            email.Clear();
            email.SendKeys("systemadministrator@tlcpetfood.com");

            // Enter the login password
            IWebElement password = driver.FindElement(By.Id("Input_Password"));
            password.Clear();
            password.SendKeys("Tlc12345!");

            // Submit login information
            IWebElement login = driver.FindElement(By.ClassName("btn"));
            login.Submit();
        }

        [Test]
        public void test()
        {
            // Find the "Supply Chain" menu item and click on it
            IWebElement SupplyChain = driver.FindElement(By.LinkText("Supply Chain"));
            SupplyChain.Click();

            // Find the "Vehicles" menu item and click on it
            IWebElement VehicleList = driver.FindElement(By.LinkText("Vehicles"));
            VehicleList.Click();

            // Begin adding a Vehicle by clicking on the "Add New Vehicle" link
            IWebElement VehicleAdd = driver.FindElement(By.LinkText("Add New Vehicle"));
            VehicleAdd.Click();

            // Add a Vehicle Name
            IWebElement VehicleAddName = driver.FindElement(By.Id("Input_Name"));
            VehicleAddName.Clear();
            VehicleAddName.SendKeys("Automated Test Vehicle");

            // Add a vehicle make
            IWebElement VehicleMake = driver.FindElement(By.Id("Input_Make"));
            VehicleMake.Clear();
            VehicleMake.SendKeys("Ford");

            // Add a vehicle model
            IWebElement VehicleModel = driver.FindElement(By.Id("Input_Model"));
            VehicleModel.Clear();
            VehicleModel.SendKeys("Fusion");

            // Add the vehicle year
            IWebElement VehicleAddYear = driver.FindElement(By.Id("Input_Year"));
            VehicleAddYear.Clear();
            VehicleAddYear.SendKeys("2016");

            // Add a note
            IWebElement VehicleNote = driver.FindElement(By.Id("Input_Notes"));
            VehicleNote.Clear();
            VehicleNote.SendKeys("Dave's awesome car.");

            // Add a license plate
            IWebElement VehiclePlate = driver.FindElement(By.Id("Input_LicensePlate"));
            VehiclePlate.Clear();
            VehiclePlate.SendKeys("CKAD 055");

            // Add the mileage
            IWebElement VehicleMiles = driver.FindElement(By.Id("Input_Odometer"));
            VehicleMiles.Clear();
            VehicleMiles.SendKeys("55000");

            // Associate the vehicle with NH DC
            IWebElement VehicleDC = driver.FindElement(By.Id("Input_DistributionCenterID"));
            SelectElement selectElement = new SelectElement(VehicleDC);
            selectElement.SelectByText("New Hamburg");

            // Submit the vehicle info and create it
            IWebElement submit = driver.FindElement(By.ClassName("btn-primary"));
            submit.Click();

            IWebElement VehicleTitle = driver.FindElement(By.TagName("h2"));

            String VehicleText = VehicleTitle.Text;

            Assert.AreEqual("Automated Test Vehicle", VehicleText);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
